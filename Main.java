import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {

    /**
     *  Write a program that, given an array A[] of n numbers and another number x, determines whether or not there exist
     *  two elements in S whose sum is exactly x.
     */
    public static void main(String[] args) {
        int[] arr = {1, 4, 45, 6, 10, -8};
        int sum = 16;
        int size_arr = arr.length;
        System.out.println(checkPairSumWayOne(arr, size_arr, sum));
        System.out.println(checkPairSumWayTwo(arr, size_arr, sum));
        checkPairSumWayThree(arr, size_arr, sum);
    }

    static boolean checkPairSumWayOne(int[] arr, int size_arr, int sum) {
        for (int i = 0; i < size_arr - 1; i++)
            for (int j = i + 1; j < size_arr; j++)
                if (arr[i] + arr[j] == sum)
                    return true;
        return false;
    }

    static boolean checkPairSumWayTwo(int[] arr, int size_arr, int sum) {
        Arrays.sort(arr);
        int l = 0, r = size_arr - 1;
        while (l < r) {
            if (arr[l] + arr[r] == sum)
                return true;
            else if (arr[l] + arr[r] > sum)
                r--;
            else
                l++;
        }
        return false;
    }

    static void checkPairSumWayThree(int[] arr, int size_arr, int sum) {
        Set<Integer> s = new HashSet<>();

        for (int i = 0; i < size_arr; i++) {
            int tmp = sum - arr[i];
            if (s.contains(tmp) && tmp > 0)
                System.out.println("Cap gia tri thoa: (" + arr[i] + ", " + tmp + ")"  );
            s.add(arr[i]);
        }
    }

}
